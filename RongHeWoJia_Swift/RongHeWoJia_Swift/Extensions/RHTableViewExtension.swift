//
//  RHTableViewExtension.swift
//  RongHeWoJia_Swift
//
//  Created by shanyiyuan on 2020/10/18.
//  Copyright © 2020 苏州蜜巢计算机系统有限公司. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func getCellWithCellNibName( nibName:String) -> UITableViewCell {
        
        var nibName = nibName
        if nibName.isEmpty {
            nibName = "UITABLEVIEWCELL"
        }
        var cell = self.dequeueReusableCell(withIdentifier: nibName)
        if cell == nil {
            if nibName.uppercased() == "UITABLEVIEWCELL" {
                cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: nibName)
            }else{
                let nibs:[UITableViewCell] = Bundle.main.loadNibNamed(nibName, owner: self, options: nil) as! [UITableViewCell]
                
                for oneObject:UITableViewCell in nibs {

                    let className:AnyObject = classWithString(className: nibName)
                    
                    if oneObject.isKind(of:className as! AnyClass){
                        cell = oneObject
                    }
                }
            }
        }
        return cell!
    }
}
